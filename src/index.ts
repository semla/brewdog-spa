import { Beer } from "./beer";
import { getAllBeers, getBeersWithName } from "./fetch-beers";
import { Router, TemplateType } from './router';
// let beers: Beer[] = [];

export function navigateToBeer(searchString:string) {

  // not using history since page should be reloaded
  // const loc = window.location;
  // const newUrl = loc.protocol + '//' + loc.host + loc.pathname + searchString;
  // window.history.pushState ('', '', newUrl );

  const loc = window.location;
  const newUrl = loc.protocol + '//' + loc.host + '/' + searchString;
  window.location.href = newUrl;
}

export function searchBeers(searchString:string, templateType: TemplateType) {
  getBeersWithName(searchString).then(beersFetched => {
    renderBeers(beersFetched, templateType);
  });
}

function renderBeers(beers: Beer[], templateType?: TemplateType) {
  resetBeforeNewPage();
  let templateEl: HTMLTemplateElement;
  
  if (templateType.id === 'Root') {
    document.getElementById('page-title').textContent = 'List of beers';
    templateEl = document.getElementById('list') as HTMLTemplateElement;
  } else if (templateType.id === 'BeerDetails') {
    document.getElementById('page-title').textContent = 'Search Results';
    templateEl = document.getElementById('details') as HTMLTemplateElement;
  }
  beers.forEach(beer => {
    // console.log(beer.name);
    // let b = document.createElement("li");
    // b.innerHTML = beer.name;
    let cBeer = new Beer(beer);
    // document.querySelector("#beerlist").appendChild(cBeer.render());
    //const templateEl = document.querySelector("#details") as HTMLTemplateElement;
    
    // templateEl.appendChild(cBeer.render( templateEl));
    
    let t = cBeer.render(templateEl, templateType);
    console.info(t)
    document.getElementById('beerlist').appendChild(t);
    // let copy = templateEl.cloneNode();
    // copy.replaceChild
  });
}

window.onload = () => {
  const router = new Router(window.location.pathname)
    
  if (router.currentRoute.templateType.id ==='Root') {
    getAllBeers()
      .then(beersFetched => {
        // beers = beersFetched;
        renderBeers(beersFetched, router.currentRoute.templateType);
      })
      .catch(r =>  alert(r))
  } else if (router.currentRoute.templateType.id === 'BeerDetails') {
    searchBeers(router.currentRoute.matchedDataInUrl.substring(1), router.currentRoute.templateType);
  }
  
  
  const form: HTMLFormElement = document.querySelector('#search-form');
  form.onsubmit = () => {
    const formData = new FormData(form);
    const searchString = formData.get('beer-searched') as string;
    // searchBeers( searchString );
    navigateToBeer(searchString);
    return false;
  }
  // autocomplete
  const input = form.querySelector("input[name='beer-searched']");
  // input.addEventListener("input", (t) => {
    
  // })
}

function resetBeforeNewPage(){
  // todo: reset to initial state with templates
  // document.querySelector("#beerlist").textContent = '';
}
