interface Route {
    path: RegExp | string; //string;
    matchedDataInUrl?: string;
    templateType: TemplateType;
}

export interface TemplateType {
    id: 'Root' | 'About' | 'BeerDetails';
}

export class Router {
    currentRoute: Route;
    routerRegex: RegExp = new RegExp(/^\/([\w|-]*)/gi);
    constructor(pathName  ) {
        // alert(pathName)
        this.currentRoute = this.matchRouteAndLoadTemplate(pathName)
        // alert(this.currentRoute.path)
    }
    
    matchRouteAndLoadTemplate(pathName:string): Route {
        
        // only one path for now
        // if (pathName.split('/').length > 1)
        //     pathName = pathName.split('/').splice(-1)[0];
        
        // let matchedRoute: Route[] = this.routes.filter(route => route.path === pathName)
        // let matchedRoute: Route[] = this.routes.filter(route => {
        //     // add regex
        //     let x = pathName.match( route.path );
            
        // })
 
        // if (matchedRoute.length < 1)
        //     matchedRoute = [this.routes[this.routes.length-1]]; // matches last path, which should be a beer

        // return matchedRoute[0];

        return this.routes.find( route => {
            // let x = pathName.match( route.path );
            let r = new RegExp(route.path)
            let x = r.exec(pathName)

            if (x) {
                route.matchedDataInUrl = x[0];
                return route;
            } else
                return false;
        } );
        
       
    };
    
    routes: Route[] = [
        {
            path: /^\/$/gi,
            templateType: {id:'Root'}
        },
        {
            path: '/about',
            templateType: {id: 'About'},
        },
        {
            path: /^\/([\w|-]*)/gi, // '/beer',
            templateType: {id: 'BeerDetails'},
        },
    ];
}
