import {getAllBeers} from '../src/fetch-beers';
import {mockResponse} from './beer-response-mock';
import {Beer} from './beer.interface';

describe("Get beers from mockdata", async function() {
    it("Should return beers", async () => {
        // const beerData = await getAllBeers(); // no fetch on node..
        const beerData = mockResponse;
        expect(beerData[0].id).toBeCloseTo(1);
        expect(beerData.length).toBeGreaterThan(2);
    });
});


